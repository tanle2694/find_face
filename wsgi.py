from app import app
import logging
from logging.handlers import RotatingFileHandler
import settings

if __name__ != "__main__":

    app.log = logging.getLogger("werkzeug")
    handler = logging.handlers.TimedRotatingFileHandler(settings.APP_LOG, when="midnight", backupCount=10)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)


if __name__ == "__main__":

    app.run(host="0.0.0.0", port=5011, debug=True)
