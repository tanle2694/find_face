from MongoDBManagement import MongoDBManagement
import time
import face_model
import cv2
import os
import faiss
import numpy as np
import settings
import redis
import json
import helper
from src.logger import  Logger

d = settings.NB_D
res = faiss.StandardGpuResources()
flat_config = faiss.GpuIndexFlatConfig()
flat_config.device = 0
class SearchService:
    def __init__(self):

        self.db = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=settings.REDIS_DB)
        self.logger = Logger(log_file_name="logs/search_service/search_service_log").logger

    def check_queue(self):
        while True:
            queue  = self.db.lrange(settings.SEARCH_QUEUE, 0, - 1)
            if len(queue) == 0:
                time.sleep(settings.SERVER_SLEEP)
                continue

            data_dict = dict()
            for q in queue:
                q = json.loads(q.decode("utf-8"))
                company_name = q["company_name"]
                if not data_dict.has_key(company_name):
                    data_dict[company_name] = []
                data_dict[company_name].append(q)

            for company_name in data_dict.keys():
                self.logger.debug("Company name: " + company_name)
                mongo = MongoDBManagement(client=company_name)
                datas, paths, ids = mongo.get_all_vector()
                datas = np.array(datas, dtype=np.float32)

                index = None

                if datas.shape[0] != 0:
                    index = faiss.GpuIndexFlatL2(res, d, flat_config)
                    index.add(datas)
                print(datas.shape)
                for j in data_dict[company_name]:
                    uid = j["uid"]
                    number_search = int(j["nb_search"])
                    if index is None:
                        self.db.set(uid, 500)
                        self.logger.debug("UID: " + uid +" database empty")
                        continue
                    embs = helper.base64_decode_image(j["emb"],"float32", (1, settings.NB_D ))
                    print(number_search)
                    print(type(number_search))
                    print(type(embs))
                    distances, ids_search = index.search(embs, number_search)


                    embs_find = np.array(datas[ids_search[0]])
                    similarity = np.dot(embs[0], embs_find.T)
                    similarity_return = ""
                    path_return = ""
                    id_return = ""

                    for (i, i_search) in enumerate(ids_search[0]):
                        similarity_return = similarity_return + "|" + str(similarity[i])
                        path_return = path_return + "|" + paths[i_search]
                        id_return = id_return + "|" + str(ids[i_search])
                    d_j = {"similarity": similarity_return, "path_return": path_return, "id_return": id_return}
                    self.logger.debug(json.dumps(d_j))
                    self.db.set(uid, json.dumps(d_j))
                mongo.client.close()
            self.db.ltrim(settings.SEARCH_QUEUE, len(queue), -1)

search = SearchService()
try:
    search.check_queue()
except Exception as e:
    search.logger.error(str(e), exc_info = True)


