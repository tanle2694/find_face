
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import ssl
import settings
ALLOW_EXTENSION = ['JPG', 'JPEG', 'PNG', 'AVI', 'MP4', 'MPEG']
# Create custom HTTPRequestHandler class
class my_HTTPRequestHandler(BaseHTTPRequestHandler):
    # handle GET command
    def do_GET(self):

        try:
            extension = self.path.split('.')[-1].upper()
            print(extension)
            print(extension in ALLOW_EXTENSION)
            if (extension in ALLOW_EXTENSION):
                print()
                f = open(rootdir + self.path)  # open requested file

                # send code 200 response
                self.send_response(200)

                # send header first
                self.send_header('Content-type', 'text-html')
                self.end_headers()

                # send file content to client
                self.wfile.write(f.read())
                f.close()
                return
            else:
                self.send_error(404, 'Permision denied')

        except Exception as e:
            print(e)
            self.send_error(404, 'file not found')


def run():
    print('http server is starting...')

    # ip and port of servr
    # by default http server port is 80
    server_address = (address, PORT)
    httpd = HTTPServer(server_address, my_HTTPRequestHandler)
    httpd.socket = ssl.wrap_socket(httpd.socket, certfile='/etc/nginx/datasection.com.vn.cert+ca-bundle.cert',
                    keyfile="/etc/nginx/datasection.com.vn.key", server_side=True)
    print('http server is running...')
    httpd.serve_forever()


if __name__ == '__main__':
    rootdir = settings.SAVE_DATABASE
    PORT = settings.PORT
    address = settings.ADDRESS
    run()