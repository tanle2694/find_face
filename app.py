from flask import Flask, render_template, json, request, redirect, session, jsonify

import os
import uuid
import utils
import numpy as np
import cv2
import time
from datetime import datetime
import  settings
import utils
from flask import Response
import helper
from werkzeug.utils import secure_filename

import logging
from logging.handlers import RotatingFileHandler
app = Flask(__name__)


app.secret_key = 'spooky action at a distance-Einstein'

app.config['UPLOAD_FOLDER'] = 'static/Uploads'
IMAGE_EXTENSIONS = set(['JPG', 'JPEG', 'PNG'])
VIDEO_EXTENSIONS = set(['MP4', 'AVI'])

def allowed_file(file_name):
    if file_name.rsplit('.', 1)[1].upper() in VIDEO_EXTENSIONS:
        return 1
    if file_name.rsplit('.', 1)[1].upper() in IMAGE_EXTENSIONS:
        return -1
    return 0


@app.route('/search', methods=['POST'])
def upload():
    time_current = "[ " + str(datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d  %H-%M-%S")) + " ]  "
    app.logger.warning(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method + " Form: " + str(request.form.to_dict()))
    print(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method + " Form: " + str(request.form.to_dict()))
    form_dict = request.form.to_dict()
    metadata = form_dict['metadata']
    json_data = json.loads(metadata)
    dataset = json_data["dataset_namespace"]
    number_search = int(json_data["limit"])
    try:
        if request.method == 'POST':
            folder_name = str(uuid.uuid4())
            path = datetime.fromtimestamp(time.time()).strftime("%Y/%m/%d")
            path = os.path.join(settings.SAVE_SEARCH, dataset, path, folder_name)
            os.makedirs(path)
            print(path)

            files = request.files.getlist('data')

            print(files)
            for file in files:
                al = allowed_file(file.filename)
                if al == 0:
                    response = json.dumps({"Message": "Invalid extension"})
                    resp = Response(response, status=settings.INVALID_EXTENSION_CODE)
                    resp.headers['Access-Control-Allow-Origin'] = '*'
                    return resp
                filename = secure_filename(file.filename)
                file.save(os.path.join(path, filename))
                print(filename)
                app.logger.info("File name: " + filename + "\n")

            img_raw = []
            for filename in os.listdir(path):
                print(filename.split('.')[-1])
                if filename.split('.')[-1].upper() in IMAGE_EXTENSIONS:
                    img = cv2.imread(os.path.join(path, filename))
                    img_raw.append(img)
                else:
                    cap = cv2.VideoCapture(os.path.join(path, filename))
                    length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                    for i in range(length):
                        ret, frame = cap.read()
                        img_raw.append(frame)
            print("before get face embs")
            print(img_raw)
            embs = utils.get_face_embs(img_raw)
            embs_clean = []

            for (i, emb) in enumerate(embs):
                if (emb is not None) and (emb != "NoneFace"):
                    embs_clean.append(emb)
            if len(embs_clean) == 0:
                print("None face")
            result_search = utils.search_embs(embs_clean, dataset, number_search)



            dict_keyLink_valueSimilarityAndID = {}
            for result in result_search:
                json_result = result
                similarity = json_result["similarity"].split("|")[1: ]
                path_return = json_result["path_return"].split("|")[1: ]
                id_return = json_result["id_return"].split("|")[1: ]
                print(path_return)
                for i in range(len(path_return)):
                    if not dict_keyLink_valueSimilarityAndID.has_key(path_return[i]):
                        dict_keyLink_valueSimilarityAndID[path_return[i]] = {}
                        dict_keyLink_valueSimilarityAndID[path_return[i]]["similarity"] = 0
                        dict_keyLink_valueSimilarityAndID[path_return[i]]["id"] = id_return[i]

                    if dict_keyLink_valueSimilarityAndID[path_return[i]]["similarity"] < similarity[i]:
                        dict_keyLink_valueSimilarityAndID[path_return[i]]["similarity"] = similarity[i]

            sims = []
            paths = []
            for path in dict_keyLink_valueSimilarityAndID.keys():
                paths.append(path)
                sims.append(float(dict_keyLink_valueSimilarityAndID[path]["similarity"]))


            sims = np.array(sims)
            sims_sort = sims.argsort()[(-1)*number_search:][::-1]

            response_list = []

            for i in range(sims_sort.shape[0]):
                index = sims_sort[i]
                path = paths[index]
                id = dict_keyLink_valueSimilarityAndID[path]["id"]
                http_path = settings.HTTP_URL + "/".join(path.split("/")[2:])
                response_list.append({"similarity": round(sims[index]*100, 2), "path": http_path, "id": id})

            response = json.dumps(response_list)

            resp = Response(response, status=200, mimetype='application/json')

            resp.headers['Access-Control-Allow-Origin'] = '*'
            return resp
    except Exception as e:
        print(str(e))
        app.logger.error("EXCEPTION: \n" + str(e), exc_info=True)
        resp = Response(status=500, mimetype='application/json')
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    resp = Response(status=405, mimetype='application/json')
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/update_database' , methods=['POST'])
def update():
    form_dict = request.form.to_dict()
    metadata = form_dict['metadata']
    json_data = json.loads(metadata)
    dataset = json_data["dataset_namespace"]

    max_tracking = utils.get_max_tracking(dataset)

    path = datetime.fromtimestamp(time.time()).strftime("%Y/%m/%d")
    path = os.path.join(settings.SAVE_DATABASE, dataset, path, str(max_tracking))

    try:
        os.makedirs(path)
    except Exception:
        pass
    print(path)
    files = request.files.getlist('data')
    for file in files:

        al = allowed_file(file.filename)
        if al == 0:
            return Response(status=settings.INVALID_EXTENSION_CODE)
        filename = secure_filename(file.filename)
        file.save(os.path.join(path, filename))
        print(filename)
    img_raw = []
    links = []
    for filename in os.listdir(path):
        if filename.split('.')[-1].upper() in IMAGE_EXTENSIONS:
            img = cv2.imread(os.path.join(path, filename))
            img_raw.append(img)
            links.append(os.path.join(path, filename))
        else:
            cap = cv2.VideoCapture(os.path.join(path, filename))
            length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            for i in range(length):
                ret, frame = cap.read()
                img_raw.append(frame)
                links.append(os.path.join(path, filename))

    embs = utils.get_face_embs(img_raw)
    nb_success, sum_uids = utils.insert_to_db(dataset, max_tracking, embs, links)
    print("Number success:" + str(nb_success))
    print("Number uids:" + str(sum_uids))

    return Response( status=200, mimetype='application/json')

@app.route('/')
def showAddBlog():
    time_current = "[ " + str(datetime.fromtimestamp(time.time()).strftime("%Y-%m-%d  %H-%M-%S")) + " ]  "
    print(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method)
    app.logger.debug(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method + "DEBUG")
    app.logger.info(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method + "INFOR")
    app.logger.warning(time_current + " From IP: " + str(request.remote_addr) + " - " + request.method + "WARNING")
    return render_template('addBlog.html')


@app.route('/test_update_database', methods=['POST'])
def test():
    resp = Response("CORS test")
    resp.headers['Access-Control-Allow-Origin'] = '*'
    form_dict = request.form.to_dict()
    print(form_dict)
    metadata = form_dict['metadata']
    json_data = json.loads(metadata)
    dataset = json_data["dataset_namespace"]
    print(request.files)

    print(dataset)
    files = request.files.getlist('data')
    for file in files:
        filename = secure_filename(file.filename)
        print(filename)
        file.save(os.path.join("static/path_test", filename))

    return resp
#
# if __name__ == "__main__":
#     app.run(host="192.168.0.50", port=5011, debug=True)
