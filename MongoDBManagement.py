from pprint import pprint
from pymongo import MongoClient
from pymongo import ReturnDocument
from bson.objectid import ObjectId
import numpy as np
import time
from pprint import pprint
from datetime import datetime, timedelta
class MongoDBManagement:
    def __init__(self, local_host='localhost', port=27017, client='face_database', database_feature='db_feature',
                 database_max_tracking='db_max_tracking'):
        self.client = MongoClient(local_host, port)
        self.db = self.client[client]
        self.collections_feature = self.db[database_feature]
        self.collections_max_tracking = self.db[database_max_tracking]
        self.db_name = client
        self.db_name_feature = database_feature
        self.db_name_max_tracking = database_max_tracking

    def reset_db(self):

        # delete all database
        self.client.drop_database(self.db_name)
        self.db = self.client[self.db_name]

        # Init database
        self.collections_feature = self.db[self.db_name_feature]
        self.collections_max_tracking = self.db[self.db_name_max_tracking]

        if self.collections_max_tracking.find({}).count() == 0:
            self.collections_max_tracking.insert_one({"max_tracking": 0})



    # Manage feature of image ---------------------------------------------------------------------------------------
    def insert_record(self, tracking_id, paths, in_features):
        records = []

        for i in range(len(paths)):
            # print(in_features[i].shape)
            f = in_features[i].tolist()

            records.append(
                {
                    "tracking_id": tracking_id,
                    "in_path": paths[i],
                    "in_feature": f
                })

        if len(records) > 0:
            self.collections_feature.insert_many(records)



    def insert_max_tracking(self, max_tracking):
        self.collections_max_tracking.insert_one({"max_tracking": max_tracking})


    def get_all_vector(self):
        feature_and_paths = list(self.collections_feature.find({ },
                                          { "tracking_id": 1,
                                            "in_path": 1,
                                            "in_feature": 1,
                                            "_id": 0
                                          }))
        paths = []
        features = []
        users_id = []

        for element in feature_and_paths:
            paths.append(element['in_path'])
            features.append(element['in_feature'])
            users_id.append(element['tracking_id'])

        return features, paths, users_id


    def count_number_of_vector_db(self):
        number_of_vector = self.collections_feature.find({}).count()
        return number_of_vector

    # -- MAX TRACKING ID ------------------------------------------------------------------

    def increase_max_tracking_id(self):

        self.collections_max_tracking.update({}, {"$inc": {"max_tracking": 1}})

    def get_max_tracking_id(self):
        if self.collections_max_tracking.find({}).count() == 0:
            self.collections_max_tracking.insert_one({"max_tracking": 0})
        # self.update_max_tracking_id(shop_id)
        max_tracking_id = self.collections_max_tracking.find_one({},
                                                             {"max_tracking": 1, "_id": 0})

        return max_tracking_id['max_tracking']




