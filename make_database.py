import os
import shutil

folder = "/data/app/crawler/facebook/data_vietnam/data_160"
folder_save = "static/facebook_db"
number_folder = 0
for sub_folder in os.listdir(folder):
    print(number_folder)
    number_folder += 1
    number_image = 0
    os.makedirs(os.path.join(folder_save, sub_folder))
    for image in os.listdir(os.path.join(folder, sub_folder)):
        link_image = os.path.join(folder, sub_folder, image)
        shutil.copy(link_image, os.path.join(folder_save, sub_folder, image))
        number_image += 1
        if number_image == 6:
            break
