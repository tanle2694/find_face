
export PYTHONPATH=/home/dsvn/caffe/python:/home/dsvn/tanlm/faiss/faiss:$PYTHONPATH

python face_service.py &
python db_service.py &
python search_service.py &
/usr/local/bin/gunicorn --certfile /etc/nginx/datasection.com.vn.cert+ca-bundle.cert --keyfile /etc/nginx/datasection.com.vn.key -b 0.0.0.0:5011 -w 5 wsgi:app &
python http_server.py &
#cd static/database && python http_server.py &
