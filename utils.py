import numpy as np
import settings
import redis
import uuid
import helper
import json
import time

db = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=settings.REDIS_DB)



def get_max_tracking(company_name):
    uid = str(uuid.uuid4())
    d = {"uid": uid, "company_name": company_name}
    db.rpush(settings.MAXTRACKING_QUEUE, json.dumps(d))
    start  = time.time()
    print("start get max tracking")
    while True:
        output = db.get(uid)
        print(output)
        if output is not None:
            max_tracking = int(output)
            return max_tracking
        time.sleep(settings.CLIENT_SLEEP)
        if (time.time() - start) > 120:
            return "Error"

def insert_to_db(company_name, id, embs, links):
    uids = []
    for i in range(len(embs)):
        if (embs[i] is None) or (embs[i] == "NoneFace"):
            continue
        uid = str(uuid.uuid4())
        emb_string = helper.base64_encode_image(embs[i])
        print(id, links[i])
        d = {"uid": uid, 'id': id, 'link': links[i] , "emb": emb_string, 'company_name': company_name}
        db.rpush(settings.INSERT_QUEUE, json.dumps(d))
        uids.append(uid)
    number_success = 0
    while True:
        if len(uids) == 0:
            break
        list_uid = uids[:]

        for uid in list_uid:
            output = db.get(uid)
            if output is not None:
                output = output.decode("utf-8")
                if int(output) == 200:
                    number_success += 1
                    uids.remove(uid)
        time.sleep(settings.CLIENT_SLEEP)
    return number_success, len(uids)

def get_face_embs(images_raw):
    print("start get embs")
    uids = []
    uid_order_dict = dict()
    print(images_raw)
    for (i, image_raw) in enumerate(images_raw):
        print("images_raw")
        uid = str(uuid.uuid4())
        image_to_str = helper.base64_encode_image(image_raw)
        height = image_raw.shape[0]
        width = image_raw.shape[1]
        d = {"uid": uid, "image": image_to_str, "height": str(height), "width": str(width)}
        db.rpush(settings.IMAGE_QUEUE, json.dumps(d))
        uids.append(uid)
        uid_order_dict[uid] = i

    time_start = time.time()
    embs = [None for i in range(len(uids))]
    print("waiting")
    while True:
        if len(uids) == 0:
            break
        list_uid = uids[:]
        for uid in list_uid:
            output = db.get(uid)

            if output is not None:
                output = output.decode("utf-8")
                if output != "NoneFace":
                    emb = helper.base64_decode_image(output, 'float32', (settings.NB_D, ))
                else:
                    emb = "NoneFace"
                embs[uid_order_dict[uid]] = emb
                uids.remove(uid)

        time.sleep(settings.CLIENT_SLEEP)

        if (time.time() - time_start) > 120:
            break
    print("complete")
    return embs

def search_embs(embs, company_name, number_search):
    print("start search embs")
    uids = []
    uid_order_dict = dict()
    for (i, emb) in enumerate(embs):
        uid = str(uuid.uuid4())
        emb_to_str = helper.base64_encode_image(emb)
        d = {"company_name": company_name, "uid": uid, "emb": emb_to_str ,"nb_search": str(number_search)}
        db.rpush(settings.SEARCH_QUEUE, json.dumps(d))
        uids.append(uid)
        uid_order_dict[uid] = i

    response = [None for i in range(len(uids))]
    time_start = time.time()
    while True:
        if len(uids) == 0:
            break
        list_uid = uids[:]
        for uid in list_uid:
            output = db.get(uid)
            print(output)
            if output is not None:
                output = output.decode("utf-8")
                if str(output) != "500":
                    return_json = json.loads(output)
                    response[uid_order_dict[uid]] = return_json
                else:
                    response[uid_order_dict[uid]] = "database_empty"

                uids.remove(uid)

        time.sleep(settings.CLIENT_SLEEP)

        if (time.time() - time_start) > 120:
            break

    return response








