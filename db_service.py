from MongoDBManagement import MongoDBManagement
import settings
import redis
import json
import time
import helper
from multiprocessing import Process
from src.logger import Logger


def manage_get_max_tracking():
    print("start manage get max tracking")
    logger = Logger(log_file_name="logs/manage_get_max_tracking/get_maxtracking_log").logger
    try:
        db = redis.StrictRedis(host=settings.REDIS_HOST,
                                    port=settings.REDIS_PORT, db=settings.REDIS_DB)
        logger.debug("Connect sucess redis db")
        while True:
            queue = db.lrange(settings.MAXTRACKING_QUEUE, 0, - 1)

            if len(queue) == 0:
                time.sleep(settings.SERVER_SLEEP)
                continue
            company_name_uid_dict = dict()
            for q in queue:
                q = json.loads(q.decode("utf-8"))
                company_name = q["company_name"]
                uid = q["uid"]
                if not company_name_uid_dict.has_key(company_name):
                    company_name_uid_dict[company_name] = []
                company_name_uid_dict[company_name].append(uid)
            for company_name in company_name_uid_dict.keys():
                mongo = MongoDBManagement(client=company_name)
                logger.debug("Company name: " + company_name)
                for uid in company_name_uid_dict[company_name]:
                    max_tracking = mongo.get_max_tracking_id()
                    db.set(uid, max_tracking)
                    logger.debug("uid: " + str(uid) + " max_tracking: " + str(max_tracking))
                    mongo.increase_max_tracking_id()
                mongo.client.close()
            db.ltrim(settings.MAXTRACKING_QUEUE, len(queue), -1)
    except Exception as e:
        logger.error("EXCEPTION: \n" + str(e), exc_info=True)


def manage_insert_tracking():
    print("start manage insert tracking")
    logger = Logger(log_file_name="logs/manage_insert_tracking/insert_tracking_log").logger

    try:
        db = redis.StrictRedis(host=settings.REDIS_HOST,
                               port=settings.REDIS_PORT, db=settings.REDIS_DB)
        logger.debug("Connect sucess redis db")
        while True:
            queue = db.lrange(settings.INSERT_QUEUE, 0, - 1)
            if len(queue) == 0:
                time.sleep(settings.SERVER_SLEEP)
                continue
            data_dict = dict()
            for q in queue:
                q = json.loads(q.decode("utf-8"))
                company_name = q["company_name"]
                if not data_dict.has_key(company_name):
                    data_dict[company_name] = []
                data_dict[company_name].append(q)
            for company_name in data_dict.keys():
                logger.debug("Insert company name: " + company_name)
                mongo = MongoDBManagement(client=company_name)
                for j in data_dict[company_name]:
                    uid =j["uid"]
                    id = j["id"]
                    link = j["link"]
                    emb = helper.base64_decode_image(j["emb"],"float32", (1, settings.NB_D ))
                    mongo.insert_record(int(id), [link], emb)
                    logger.debug("Success:  id: " + str(id) + "link: " + link + "emb: " + str(emb))
                    db.set(uid, 200)

                mongo.client.close()
            db.ltrim(settings.INSERT_QUEUE, len(queue), -1)

    except Exception as e:
        logger.error("Exception: \n" + str(e), exc_infor= True)

if __name__ == "__main__":

    p1 = Process(target=manage_get_max_tracking, args=())
    p2 = Process(target=manage_insert_tracking, args=())
    p1.start()
    p2.start()
    p1.join()
    p2.join()