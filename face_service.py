from MongoDBManagement import MongoDBManagement
import time
import face_model
import cv2
import os
import numpy as np
import settings
import redis
import json
import helper
from src.logger import  Logger

class face_service:

    def __init__(self):

        self.db = redis.StrictRedis(host=settings.REDIS_HOST,
                               port=settings.REDIS_PORT, db=settings.REDIS_DB)
        self.db.flushall()
        self.model = face_model.FaceModel()
        self.logger = Logger(log_file_name="logs/face_service/face_service_log").logger


    def check_queue(self):
        imageIDS = []
        batch = []
        while True:
            queue  = self.db.lrange(settings.IMAGE_QUEUE, 0, settings.BATCH_SIZE - 1)
            if len(queue) == 0:
                time.sleep(settings.SERVER_SLEEP)
                continue
            for q in queue:
                q = json.loads(q.decode("utf-8"))
                image = helper.base64_decode_image(q["image"], settings.IMAGE_DTYPE, (int(q["height"]),
                                                                                      int(q["width"]), settings.IMAGE_CHANS))
                batch.append(image)
                imageIDS.append(q["uid"])
            self.logger.debug("UIDs: " + str(imageIDS))
            break
        return imageIDS, batch


    def face_detection_and_emb(self, batch, imageIDS):
        faces = []
        ids_has_face = []
        ids_none_face = []
        print("before search face")
        print(imageIDS)
        print("----")
        for i in range(len(batch)):
            self.logger.debug(batch[i].shape)

            face = self.model.get_input(batch[i])
            if face is None:
                self.logger.debug("uid: " + str(imageIDS[i]) + " has none face")
                ids_none_face.append(imageIDS[i])
            else:
                ids_has_face.append(imageIDS[i])
                faces.append(face)
        embs = []
        if len(faces) > 0:
            self.logger.debug("Get feature")
            embs = self.model.get_feature(faces)
        else:
            self.logger.debug("Don't have any face")
        self.logger.debug("Number embs: " + str(len(embs)))
        self.logger.debug("ids has face: " + str(ids_has_face))
        self.logger.debug("ids none face" + str(ids_none_face))
        return embs, ids_has_face, ids_none_face


    def process(self):
        imageIDS, batch = self.check_queue()
        print("before detection and emb")
        embs, ids_has_face, ids_none_face = self.face_detection_and_emb(batch, imageIDS)

        for i in range(len(ids_none_face)):
            uid_of_none_face = ids_none_face[i]
            self.db.set(uid_of_none_face, "NoneFace")
            self.logger.debug("set uid " + uid_of_none_face + " None face")

        for i in range(len(ids_has_face)):
            uid_of_has_face = ids_has_face[i]
            emb_string = helper.base64_encode_image(embs[i])
            self.db.set(uid_of_has_face, emb_string)
            self.logger.debug("set uid " + uid_of_has_face + " Has face")
        self.db.ltrim(settings.IMAGE_QUEUE, len(imageIDS), -1)



server = face_service()

while True:
    try:
        server.process()
    except Exception as e:
        server.logger.error("Exception:  \n" + str(e), exc_info=True)






