import os

CURRENT_DIR = "/data/data/"

# REDIS parametter
REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_DB = 0

# initialize constants used to control image spatial dimensions and
# data type
IMAGE_WIDTH = 112
IMAGE_HEIGHT = 112
IMAGE_CHANS = 3
IMAGE_DTYPE = "uint8"

# initialize constants used for server queuing
IMAGE_QUEUE = "image_queue"
MAXTRACKING_QUEUE = "maxtracking_queue"
INSERT_QUEUE = "insert_queue"
SEARCH_QUEUE = "search_queue"
BATCH_SIZE = 10
SERVER_SLEEP = 0.25
CLIENT_SLEEP = 0.25

# client
MAX_PUSH = 20
NB_D = 512
NUMBER_SEARCH = 5

NONE_FACE_CODE = 404
INVALID_EXTENSION_CODE = 409

# Folder save
SAVE_SEARCH = "static/Upload"
SAVE_DATABASE = "static/database"
APP_LOG = "logs/app_log/app_log.log"


# HTTPS server
PORT = 8002
ADDRESS = "192.168.0.50"
HTTP_URL = "https://public.datasection.com.vn:" + str(PORT) + "/"
