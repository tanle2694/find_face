import time
import logging
import logging.handlers
from datetime import datetime

class Logger:
    def __init__(self, log_file_name = 'logs/deeplearning_log/deeplearning_log'):
        # log_file_name = log_file_name + datetime.fromtimestamp(time.time()).strftime("_%Y_%m_%d") + '.log'
        log_file_name = log_file_name  + '.log'
        logging_level = logging.DEBUG

        # set TimedRotatingFileHandler for root
        formatter = logging.Formatter('%(asctime)s %(name)s [%(filename)s:%(lineno)d] %(levelname)s %(message)s')
        # use very short interval for this example, typical 'when' would be 'midnight' and no explicit interval
        handler = logging.handlers.TimedRotatingFileHandler(log_file_name, when="midnight", backupCount=10)
        handler.setFormatter(formatter)
        self.logger = logging.getLogger() # or pass string to give it a name
        self.logger.addHandler(handler)
        self.logger.setLevel(logging_level)
        self.logger.info('-------------------------------------------- START LOGGING -----------------------------------------')

