
import sys, os, argparse

import numpy as np
import cv2
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
import torch.backends.cudnn as cudnn
import torchvision
import torch.nn.functional as F
from PIL import Image

import datasets, hopenet, utils

class head_pose():
    def __init__(self):
        cudnn.enabled = True
        batch_size = 1
        self.gpu = 0
        snapshot_path = "head_pose/hopenet_alpha2.pkl"

        self.model = hopenet.Hopenet(torchvision.models.resnet.Bottleneck, [3, 4, 6, 3], 66)

        print 'Loading snapshot.'
        # Load snapshot
        saved_state_dict = torch.load(snapshot_path)
        self.model.load_state_dict(saved_state_dict)

        print 'Loading data.'

        self.transformations = transforms.Compose([transforms.Scale(224),
                                              transforms.CenterCrop(224), transforms.ToTensor(),
                                              transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])

        self.model.cuda(self.gpu)

        print 'Ready to test network.'

        self.model.eval()  # Change model to 'eval' mode (BN uses moving mean/var).
        total = 0

        idx_tensor = [idx for idx in xrange(66)]
        self.idx_tensor = torch.FloatTensor(idx_tensor).cuda(self.gpu)

    def get_head_pose(self, frame):
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(img)

        img = self.transformations(img)
        img_shape = img.size()
        img = img.view(1, img_shape[0], img_shape[1], img_shape[2])
        img = Variable(img).cuda(self.gpu)

        yaw, pitch, roll = self.model(img)

        yaw_predicted = F.softmax(yaw)
        pitch_predicted = F.softmax(pitch)
        roll_predicted = F.softmax(roll)
        # Get continuous predictions in degrees.
        yaw_predicted = torch.sum(yaw_predicted.data[0] * self.idx_tensor) * 3 - 99
        pitch_predicted = torch.sum(pitch_predicted.data[0] * self.idx_tensor) * 3 - 99
        roll_predicted = torch.sum(roll_predicted.data[0] * self.idx_tensor) * 3 - 99
        heigh = frame.shape[0]
        width = frame.shape[1]
        if (-20 < int(yaw_predicted) < 20) and (-20 < int(pitch_predicted) < 20) and (-20 < int(roll_predicted) < 20):
            print("accept")
        else:
            print("Reject")
        utils.draw_axis(frame, yaw_predicted, pitch_predicted, roll_predicted, tdx=heigh/2, tdy=width/2, size=heigh/2)
        return frame
